# TestApp LaTeX Builder

TestApp LaTeX Builder is a JavaScript tool to create scientific LaTeX formulas using a mobile-ready WYSIWYG editor.

[Online Demo](https://testapp-system.gitlab.io/latex-builder/)

This software was initially created to the [TestApp](https://testapp.schule/) education project.

# Technical details

How we realized this:

- [MathLive](https://mathlive.io/) by [Arno Gourdol](https://github.com/arnog), MIT licensed

# License

This software is EUPL-1.2 licensed.